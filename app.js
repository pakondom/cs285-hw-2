const get_flight_data = () => {
    return new Promise((resolve, reject) => {
        const request = require('request');
        request('https://api.myjson.com/bins/f2sum', { json: true }, (err, res, body) => {
            if(err){
                reject(err);
            }else{
                resolve(body);
            }
        });
    });
};

get_flight_data().then(res => {
    if (res) {  
        var flight_data = res;

        var express = require('express');
        var app = express();
        var path = require('path');
        var port = 8080;

        const bodyParser = require('body-parser');
        app.use(bodyParser.urlencoded({ extended: true }));

        app.set('views', path.join(__dirname, 'views'));
        app.set('view engine', 'ejs');
        app.use(express.static('public'));

        app.get('/', function(req, res) {
            res.render('searchFlights');
        });

        app.get('/display', function(req, res) {
            res.render('displayFlights', {data: flight_data});
        });

        app.get("/payment", function (req, res) {
            var index = req.query.flight_id;
            var f_data = flight_data[index];
            var price_detail = {
                flight_price: f_data.price.amount,
                tax_price: 1000,
                fee_price: {
                bag: 0,
                travel: 0
                },
                discount_price: 0,
                total_price: 0
            }
            var ticketPrice = new TicketPrice(price_detail); 
            taxPrice(ticketPrice, price_detail);
            feePrice(ticketPrice, {
                bag_box_isChecked: 'false',
                life_box_isChecked: 'false'
            }, price_detail);
            discountPrice(ticketPrice, price_detail);
            price_detail.total_price = ticketPrice.total_price();
            res.render('payment', {
                data: {
                    index: index, 
                    f_data: f_data,
                    price_detail : price_detail
                }
            });
        });

        app.post("/confirm_payment", function (req, res) {
            var data = req.body;
            console.log(data);
            res.send('Thank you');
        });

        app.post("/calculate_price", function (req, res) {
            var data = req.body;
            var f_data = flight_data[data.f_index];
            var price_detail = {
                flight_price: f_data.price.amount,
                tax_price: 1000,
                fee_price: {
                bag: 0,
                travel: 0
                },
                discount_price: 0,
                total_price: 0
            }
            var ticketPrice = new TicketPrice(price_detail); 
            taxPrice(ticketPrice, price_detail);
            feePrice(ticketPrice, data.fee_detail, price_detail);
            discountPrice(ticketPrice, price_detail);
            price_detail.total_price = ticketPrice.total_price();
            res.send(price_detail);
        });


        app.listen(port, function() {
            console.log('Listening on port: ' + port);
        });
    }
}).catch(e => {
    console.log(e);
});

//Decorator calulate total price
function TicketPrice(price_detail){
    this.total_price = function(){
      return price_detail.flight_price;
    }
  }

  function taxPrice(ticketPrice, price_detail) { 
    var total = ticketPrice.total_price(); 
    ticketPrice.total_price = function() { 
      return total + price_detail.tax_price; 
    }; 
  } 

  function feePrice(ticketPrice, fee_detail, price_detail) { 
    if(fee_detail.bag_box_isChecked === "true"){
        price_detail.fee_price.bag = 350;
    }else{
        price_detail.fee_price.bag = 0;
    }
    if(fee_detail.life_box_isChecked === "true"){
        price_detail.fee_price.travel = 1300;
    }else{
        price_detail.fee_price.travel = 0;
    }
    var total = ticketPrice.total_price(); 
    ticketPrice.total_price = function() { 
      return total + price_detail.fee_price.bag + price_detail.fee_price.travel; 
    }; 
  } 

  function discountPrice(ticketPrice, price_detail) { 
    var discount = 0;
    if(price_detail.fee_price.bag != 0 && price_detail.fee_price.travel != 0){
      var fee = price_detail.fee_price.bag + price_detail.fee_price.travel;
      discount = fee * 0.1;
      price_detail.discount_price = discount;
    }else{
      price_detail.discount_price = 0;
    }  
    var total = ticketPrice.total_price(); 
    ticketPrice.total_price = function() { 
      return total - discount; 
    }; 
  } 